<!doctype html>
<?php
require ('mlib_values.php');
require('mlib_functions.php');
html_head("mlib Media Status");
require('mlib_header.php');
require('mlib_sidebar.php');


# Code for your web page follows.'

try 
{

   // open database

   $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
	// set up exception handler
   $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   // get the table from the database
   $result= $db->query("SELECT * FROM media");

   // set up the header of the html table
   print "<h2>Media Status</h2>";
   print "<table border=1>";
   print "<tr>";
   print "<td width = \"100\">Title</td>";
   print "<td width = \"150\">Author</td>";
   print "<td width = \"175\">Description</td>";
   print "<td width = \"75\">Type</td>";
   print "<td width = \"40\">User ID</td>";
   print "<td width = \"100\">Status</td>";
   print "<td width = \"100\">Date</td>";
   print "</tr>";

   // loop through each row of the database table and place it into the html table   
   foreach($result as $row)
   {
      $title = $row['title'];
      $author = $row['author'];
      $description = $row['description'];
      $type = $row['type'];
	  $user_id = $row['user_id'];
	  $status = $row['status'];
	  $date = $row['date_in'];

	  // set user_id to user name
	  if($user_id>0)
	  {
		  $result = $db->query("SELECT * FROM mlib_users where id = $user_id")->fetch();
		  $user_name = $result['first']." ".$result['last'];
	  }else
	  {
		  $user_name = "available";
		  $date = "not reserved";
	  }
      print "<tr>";
	  print "<td>" . $title . "</td>";
      print "<td>".$author."</td>";
      print "<td>".$description."</td>";
      print "<td>".$type. "</td>";
	  print "<td>".$user_name . "</td>";
	  print "<td>".$status."</td>";
	  print "<td>".$date."</td>";
      print "</tr>";
   }
   print "</table>"; 
   $db = NULL;
}
catch(PDOEXCEPTION $e)
{

   echo 'Exception : '.$e->getMessage();
   echo "<br/>";
   $db = NULL;
}
require('mlib_footer.php');
?>

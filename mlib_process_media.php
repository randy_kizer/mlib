<!doctype html>
<?php
require ('mlib_values.php');
require('mlib_functions.php');
html_head("mlib Process Media");
require('mlib_header.php');
require('mlib_sidebar.php');


// process uploaded media file

echo "<h2>Processing Media from List ...</h2>";

if ($_FILES['userfile']['error'] > 0)
{
	echo 'Problem: ';
	switch ($_FILES['userfile']['error'])
	{
		case 1: echo 'File exceeded upload_max_filesize'; break;
		case 2: echo 'File exceeded max_file_size'; break;
		case 3: echo 'File only partially uploaded'; break;
		case 4: echo 'No file uploaded'; break;
	}
	exit;
}
// Does the file have the right MIME type?
if($_FILES['userfile']['type'] != 'text/plain')
{
	echo 'Problem: file is not plain text';
	exit;
}

// put the file in the downloads sub directory
$upfile = './uploads/'.$_FILES['userfile']['name'];


if(is_uploaded_file($_FILES['userfile']['tmp_name']))
{
	if(!move_uploaded_file($_FILES['userfile']['tmp_name'], $upfile))
	{
		echo 'Problem: Could not move file to destination directory';
		exit;
	}
}
else
{
	echo 'Problem: Prossible file upload attack. Filename: ';
	echo $_FILES['username']['name'];
	exit;
}

echo 'File uploaded successfully<br><br>';

// display the content to the screen
$fp = fopen($upfile, 'r');

if (!$fp)  // did the open succeed?
{
	echo "<p>I could not open $upfile right now.";
	exit;
}


// read the data from the file
while (!feof($fp))
{
	$line = fgets($fp, 100);
	$line_array = explode(',',$line);
	$title = trim($line_array[0]);
	$author = trim($line_array[1]);
	$type = trim($line_array[2]);
	$description = trim($line_array[3]);
	
	// display the date from the file, one line at atime

	
	// validate media entries
	$errors = validate_media ($title, $author, $type, $description);

	// if no errors put data into database
	// else report errors
	if (empty($errors))
	{
		//no error display fields
		// display the date from the file, one line at atime
		echo "<br>title: $title<br>";
		echo "author: $author<br>";
		echo "type: $type<br>";
		echo "description: $description<br>";
		
		// insert entry into the database
		try
		{
			// open the database
			$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			// write into database
			$db->exec("INSERT INTO media (title, author, description, type,user_id, status)
			      VALUES ('$title', '$author', '$description', '$type', 0, 'available');");
			
			$db = NULL; // close the database
		}
		catch (PDOException $e)
		{
			echo 'Exception: '. $e->getMessage();
			echo "<br>";
			
			$db = NULL;  // close the database
		}
	}
	else
	{
		// there are error in the fields
		echo "<br>Errors found in media entry:<br>";
		echo "Title: $title<br>";
		echo "Author: $author<br>";
		echo "Type: $type<br>";
		echo "Description: $description<br>";
		foreach($errors as $error)
		{
			echo $error."<br>";
		}
	}	// end if empty(errors)
}// end while
// close file
fclose($fp);

?>
<?php
require('mlib_footer.php');
?>

<!doctype html>
<?php
session_start();
require('mlib_functions.php');
html_head("mlib logout");
require('mlib_header.php');

//save valid_user to see if we were logged in to begin with
$old_user = $_SESSION['valid_user'];
unset($_SESSION['valid_user']);
session_destroy();

// sidebar is not display until here, so the new sidebar with 
// all elements will being displayed. 
// After this point it will be the sidebar without the non admin elements
require ('mlib_sidebar.php');

print "<h2>Log out</h2>";

// checking if the original session variable is empty - meaning no one was logged in
if  (empty($old_user)) {
  print "You were not logged in to begin with.<br/>";  
} else {
  print "Logged out<br/>";
}

require('mlib_footer.php');
?>
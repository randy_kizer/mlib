<?php
# CSCI59P standard functions
function html_head($title) {
  echo '<html lang="en">';
  echo '<head>';
  echo '<meta charset="utf-8">';
  echo "<title>$title</title>";
  echo '<link rel="stylesheet" href="mlib.css">';
  echo '</head>';
  echo '<body>';
}

function try_again($str)
{
	echo $str . "<br>";
	echo '<a href="#" onclick="history.back(); return false;">
	Try Again</a>';
	exit;
}

function validate_media ($title, $author, $type, $description )
{
	$error_message = array(); // create empty error_message array.
	
	// check each field for not empty
	if (strlen($title) == 0)
	{
		array_push($error_message, "Title field must have a title name.");
	}
	
	if (strlen($author) == 0)
	{
		array_push($error_message, "Author field must have an author's name");
	}
	
	if (strlen($type) == 0)
	{
		array_push($error_message, "Type field must have a type name");
	}
	
	if(strlen($description) == 0)
	{
		array_push($error_message, "Description field must have a description");
	}
	
	// verify title field is unique in the database
	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		// check for duplicates of the title
		if(strlen($title) != 0)
		{
			$sql = "SELECT COUNT(*) FROM media WHERE title = '$title'";
			$result = $db->query($sql)->fetch(); // the count entries with same title
			// if error push onto the error message array
			if ($result[0] > 0)
			{
				array_push($error_message, "$title is not unique, must be unique.");
			}
		}
		
		// check for type being valid from type column in type table
		if (strlen($type) != 0)
		{
			$sql = "SELECT COUNT(*) FROM mlib_types WHERE type = '$type'";
			$result = $db->query($sql)->fetch(); // count number of entries
			if ($result[0] == 0)
			{
				array_push($error_message, "Media type $type is not valid.");
			}
		}
		
		$db = NULL;
	}// end try
	catch (PDOException $e)
	{
		echo 'Exception: ' . $e->getMessage();
		echo "<br>";
		$db = NULL;
	}
	
	return $error_message;
}

// Validate the date format as YYYY-MM-DD
function MyCheckDate( $postedDate ) {
   if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $postedDate, $datebit)) {
      return checkdate($datebit[2] , $datebit[3] , $datebit[1]);
   } else {
      return false;
   }
}

// Validate if the user add has valid name and email
function checkUserInfo ($last, $first, $email){
		// check each field for not empty
	if (strlen($last) == 0)
	{
		array_push($error_message, "Title field must have a title name.");
	}
	
	if (strlen($first) == 0)
	{
		array_push($error_message, "Author field must have an author's name");
	}
	
	if (strlen($email) == 0)
	{
		array_push($error_message, "Type field must have a type name");
	} 
	
		// verify first, last,and email field is unique in the database
	try
	{
		//open the database
		$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		// check for duplicates of the first name
		if(strlen($first) != 0)
		{
			$sql = "SELECT COUNT(*) FROM mlib_users WHERE first = '$first'";
			$result = $db->query($sql)->fetch(); // the count entries with same first name

			// if error push onto the error message array
			if ($result[0] > 0)
			{
				array_push($error_message, "$title is not unique, must be unique.");
			}
		}
		
		// check for type being valid from type column in type table
		if (strlen($type) != 0)
		{
			$sql = "SELECT COUNT(*) FROM mlib_types WHERE type = '$type'";
			$result = $db->query($sql)->fetch(); // count number of entries
			if ($result[0] == 0)
			{
				array_push($error_message, "Media type $type is not valid.");
			}
		}
		
		$db = NULL;
	}// end try
	catch (PDOException $e)
	{
		echo 'Exception: ' . $e->getMessage();
		echo "<br>";
		$db = NULL;
	}
	
	return $error_message;
	
}// end checkUserInfo

// Check to see if we are logged in as an admin
function we_are_not_admin()
{
  if (empty($_SESSION['valid_user'])) {
    echo "Only administrators can execute this function.<br/>";
    require('mlib_footer.php');
  	return true;
	}
}


?>

<!doctype html>
<?php
require ('mlib_values.php');
require('mlib_functions.php');
html_head("mlib Add Media");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
  exit;
}



// test if data has been submitted

if(!isset($_POST['submit'])) // if not submitted get data from the text fields
{
?>
<h2> Add Media </h2>
<form action="mlib_media.php" method="post">
  <table border="0">
    <tr bgcolor="#cccccc">
      <td width="100">Field</td>
      <td width="300">Value</td>
    </tr>
    <tr>
      <td>Title</td>
      <td align="left"><input type="text" name="title" size="35" maxlength="35">
     </td>
    </tr>
    <tr>
      <td>Author</td>
      <td align="left"><input type="text" name="author" size="35" maxlength="35">
      </td>
    </tr>
    <tr>
      <td>Description</td>
      <td align="left"><input type="text" name="description" size="70" maxlength="35">
      </td>
    </tr>
    <tr>
       <td>Type</td>
       <td align="left"><select  name="type">
	   
<?php  // create pull down for type
       try
	   {
		  //open the database
		  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);

		  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		  
		  // display all types from types table
		  $result = $db->query('select * from mlib_types');
		  
		  foreach($result as $row)
		  {
		      print "<option value =".$row['type'].">".$row['type']."</option>";
		  }
		  //close DB
		  $db=NULL;
	   }
	   catch(PDOEXCEPTION $e)
       {
           echo 'Exception : '.$e->getMessage();
           echo "<br/>";
           $db = NULL;
       }
?>
           </select>
       </td>
    </tr>
    <tr>
      <td colspan="2" align="right"><input type="submit" name="submit" value="Submit">
      </td>
    </tr>
  </table>
 </form> 
 
 <?php
}
else // else data has been submitted
{ 

    $title = $_POST['title'];
	$author = $_POST['author'];
	$description = $_POST['description'];
	$type = $_POST['type'];
	
	$errors = validate_media ($title, $author, $type, $description);
	if (!empty($errors)){
		// there are error in the fields
		echo "<br>Errors found in media entry:<br>";
		echo "Title: $title<br>";
		echo "Author: $author<br>";
		echo "Type: $type<br>";
		echo "Description: $description<br>";
		foreach($errors as $error)
		{
			echo $error."<br>";
		}
		
		try_again ( " Correct above errors");
	
	}
	else
	{
		try 
		{

			// open database
	   
			$db = new PDO(DB_PATH, DB_LOGIN, DB_PW);

			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	   
			
			$db->exec("insert into media (title, author, description, type,user_id, status) values ('$title', '$author', '$description', '$type', 0, 'available' );");

			$last_id = $db->lastInsertID();
	
			print "<h2>Media Added</h2>";
			print "<table border=1>";
			print "<tr>";
				print "<td width = \"100\">Title</td>";
				print "<td width = \"150\">Author</td>";
				print "<td width = \"175\">Description</td>";
				print "<td width = \"75\">Type</td>";
				print "<td width = \"40\">User ID</td>";
				print "<td width = \"100\">Status</td>";
				print "<td width = \"100\">Date</td>";
			print "</tr>";
	
			$row = $db->query("SELECT * FROM media where id='$last_id'")->fetch(PDO::FETCH_ASSOC);
	
			$title = $row['title'];
			$author = $row['author'];
			$description = $row['description'];
			$type = $row['type'];
			$user_id = $row['user_id'];
			$status = $row['status'];
			$date = $row['date_in'];
	
			print "<tr>";
				print "<td>" . $title . "</td>";
				print "<td>".$author."</td>";
				print "<td>".$description."</td>";
				print "<td>".$type. "</td>";
				print "<td>".$user_id . "</td>";
				print "<td>".$status."</td>";
				print "<td>".$date."</td>";
			print "</tr>";
	  
			print "</table>";

			$db = NULL;
			
		}// end of try
		catch(PDOEXCEPTION $e)
		{

			echo 'Exception : '.$e->getMessage();
			echo "<br/>";
			$db = NULL;
		}// end of catch
	} // if error
}//end of else of data has been submitted
require('mlib_footer.php');
?>

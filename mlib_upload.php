<!doctype html>
<?php
require ('mlib_values.php');
require('mlib_functions.php');
html_head("mlib Upload Media");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
  exit;
}

// Code for form to upload file
?>
<h2>Upload Media List</h2>
<form enctype="multipart/form-data"
      action = "mlib_process_media.php"
	  method=post>
	  
	  <input type = "hidden" 
	         name="MAX_FILE_SIZE"
			 value="1000">
	  Upload this file:
	  <input name="userfile" type="file">
	  <input type="submit"
	         value="Upload File">
</form>

<?php
require('mlib_footer.php');
?>
